# Usuários Casuais

Usuários pesados ​​tendem a invejar o usuário de pornografia casual, todos nós conhecemos esses personagens: "*Oh, eu posso passar a semana toda sem uma sessão de pornografia, isso realmente não me incomoda.*" Gostaríamos de ser assim. Pode ser difícil de acreditar, mas nenhum usuário gosta de ser um usuário. Nunca se esqueça: 


|   *Nenhum usuário jamais decidiu se tornar um, casual ou não, **portanto,** *

|       *Todos os usuários se sentem estúpidos, **portanto,** *

|         *Todos os usuários têm que mentir para si mesmos e para os outros em uma tentativa vã de justificar sua estupidez.*


Os fanáticos por golfe se gabam da frequência com que jogam e querem jogar, então por que os usuários se gabam do pouco que se masturbam? Se esse é o verdadeiro critério, então certamente o prêmio não é se masturbar, não é?

Se alguém lhe dissesse: "*Posso passar a semana toda sem comer cenouras e isso não me incomoda nem um pouco*", você pensaria que está falando com um maluco. Se eu gostasse de cenouras, por que iria querer passar a semana inteira sem elas? Se eu não gostasse delas, por que faria tal declaração? Portanto, quando um usuário faz um comentário sobre como sobreviver a uma semana sem uma sessão, ele está tentando se convencer - e convencer você - de que não tem problemas. Mas não haveria necessidade de fazer uma declaração se eles não tivessem problemas. Traduzido, este comentário é "*Consegui sobreviver uma semana inteira sem pornografia.*" Como todo usuário, esperando que depois disso eles pudessem sobreviver pelo resto de suas vidas. Conseguindo sobreviver apenas uma semana, você pode imaginar como a sessão deve ter sido preciosa depois de ter se sentido privado por uma semana inteira?

Se você tem uma coceira permanente, a tendência natural é coçá-la. À medida que os circuitos de recompensa se tornam cada vez mais imunes à dopamina e opioides, a tendência natural é aumentar, consumir, buscar novidades, buscar choque, etc. Existem quatro fatores principais que impedem os usuários de consumir pornografia sessão após sessão.

**Tempo.**
A maioria não tem tempo.

**Saúde.**
Para aliviar essa coceira, temos que consumir todo o material gratuito que está disponível e mais um pouco. A capacidade de lidar com esse tipo de compulsão varia com cada indivíduo e em diferentes momentos e situações de suas vidas. Isso atua como uma restrição automática.

**Disciplina.**
Imposta pela sociedade ou pelo trabalho do usuário, amigos e parentes; talvez até pelos próprios usuários, como resultado do cabo de guerra natural que ocorre na mente de cada usuário.

**Imaginação.**
A falta de imaginação minimiza o choque, a novidade e outros valores da pornografia em uma base subjetiva.

É fácil pensar em usuários "não casuais" como fracos, incapazes de entender por que outros são capazes de limitar seu consumo. No entanto, os usuários pesados ​​devem ter em mente que a maioria dos usuários casuais são simplesmente incapazes de ter sessão após sessão, exigindo muita imaginação e resistência. Alguns desses usuários "uma vez por semana" que os usuários pesados ​​tendem a invejar são fisicamente incapazes de consumir mais, ou por causa do seu trabalho, a sociedade ou o próprio ódio de se tornarem viciados não os permitem.

Pode ser vantajoso fornecer algumas explicações:

**O Não-Usuário**

Alguém que nunca caiu na armadilha, mas não deve ser complacente. Eles são não-usuários apenas por sorte ou graça de Deus. Todos os usuários estavam convencidos de que nunca ficariam viciados e alguns não-usuários continuam tendo uma sessão de vez em quando.

**O Usuário Casual**

No qual existem duas classificações básicas:

1.  O usuário que caiu na armadilha da pornografia, mas não percebeu - não inveje esses usuários. Eles estão apenas provando o néctar na boca do jarro e, com toda a probabilidade, logo serão grandes usuários. Lembre-se, assim como todos os alcoólatras começaram como bebedores casuais, também todos os usuários começam casualmente.

2.  O usuário que antes era um usuário pesado e, portanto, pensa que não pode parar. Esses usuários são os mais tristes de todos e se enquadram em várias categorias, cada uma exigindo comentários separados.

**O Usuário Uma-Vez-Por-Dia**

Se eles desfrutam de seu direito ao orgasmo, por que usar pornografia na internet apenas uma vez por dia? Se eles podem pegar ou largar, por que se preocupar? Lembre-se, o 'hábito' é - na realidade - bater a cabeça contra a parede para relaxar ao parar. O usuário uma-vez-por-dia alivia seus sintomas de abstinência por menos de uma hora por dia. Embora inconscientes, o resto do dia é passado batendo a cabeça contra a parede, fazendo isso pela maior parte de suas vidas. Eles estão usando uma vez por dia porque não podem correr o risco de serem pegos ou de prejudicar sua saúde neurológica. É fácil convencer o usuário pesado de que ele não gosta, mas significativamente mais difícil convencer um casual. Qualquer pessoa que já passou por uma tentativa de corte total sabe que é a pior tortura de todas, e quase garantido que o manterá viciado pelo resto da vida.

**O Usuário Rejeitado**

Eles exigem o direito ao orgasmo todos os dias, mas seu parceiro sexual nem sempre fica feliz em atender ao pedido. Inicialmente, eles estão usando pornografia na internet para preencher esse vazio, mas ao pegar o emocionante "toboágua", eles ficam presos em um ciclo de novidades, choque, imagens supernormais, etc. Na verdade, eles estão felizes com a rejeição de seu parceiro, pois isso fornece uma espécie de desculpa. Se a pornografia na internet dá tanto para você, por que se preocupar em ter um parceiro? Em vez disso, termine com eles. Eles nem estão gostando das sessões quando precisam "carregar" o parceiro em sua mente. Em certo ponto, eles estão esperando que seu parceiro na vida real dê a eles uma desculpa para se aventurarem nos vales sombrios da internet.

**O Usuário De Dieta Pornográfica**

Também conhecido como "*Posso parar quando quiser. Já fiz isso milhares de vezes!*"

Se eles acham que fazer dieta ajuda a deixá-los com vontade de encontrar parceiros, por que estão fazendo dieta uma vez a cada quatro dias? Ninguém pode prever o futuro, o que aconteceria se o acaso da reunião ocorresse uma hora depois da sessão programada? Além disso, se "limpar o encanamento" ocasional é bom para aliviar a tensão, por que não fazer a limpeza todos os dias? Está provado que a masturbação não é necessária para manter os órgãos genitais saudáveis ​​e que a pornografia na internet não é nem um pouco necessária. Mesmo se for esse o caso, nenhum "guru" PUA que leu sobre os danos neurológicos recomendará assistir a pornografia com superestímulo. A verdade é que eles ainda estão viciados. Embora eles tenham se livrado do vício físico, eles ainda têm o problema principal da lavagem cerebral. Eles estão esperando a cada tentativa que vão parar para sempre, mas logo caem na mesma armadilha novamente.

A maioria dos usuários realmente invejam esse tipo de usuário e pensa sobre o quão "sortudo" o usuário de dieta é capaz de controlar seu uso. No entanto, eles esquecem que quem está fazendo dieta não está controlando seu uso - quando estão usando, desejam não estar. Eles passam pelo aborrecimento de parar, então começam a se sentir privados e caem na armadilha novamente, desejando não ter feito isso. Eles obtêm o pior dos dois mundos. Se você pensar bem, isso é verdade na vida dos usuários quando têm permissão para ter uma sessão - aproveitando o direito ou desejando que não. Só quando privado é que o pornô se torna precioso. A síndrome do "fruto proibido" é um dos terríveis dilemas para os usuários. Eles nunca podem vencer porque estão se lamentando por um mito, uma ilusão. Só há uma maneira de eles vencerem: parando de se lamentar por parar com a pornografia!

**O usuário "Eu só vejo pornô por Fotos/Amador"**

Sim, todo mundo faz isso para começar, mas não é incrível como o valor médio do choque desses vídeos parece aumentar rapidamente e, antes que percebamos, estamos nos sentindo privados (tolerância)? A novidade perde a força com a pornografia amadora, então pagamos o preço por um copo de graxa e descemos o escorregador de água em direção ao ressentimento e à culpa. A pior coisa que você pode fazer é usar as fotos de seu parceiro (com aprovação, é claro) para se masturbar. Por quê? Porque no processo você está reconectando seu cérebro para a busca, procura e liberação de dopamina induzida por variedade. Quimicamente, o toboágua pornográfico no cérebro são DeltaFosB crescendo, então você terá dificuldades quando estiver com o parceiro em tempo real.

Outra armadilha nesta categoria é a pornografia "amadora" e "feita em casa". A maioria são falsos e você sabe disso, além disso, você também não vai assistir o primeiro que bater em seus olhos, em vez disso vai continuar a procurar e pesquisar. Lembre-se de que não é apenas o orgasmo que o cérebro busca, mas a novidade da caçada que dá ao toboágua sua emoção. O conteúdo pornográfico não é o problema - seja amador ou profissional - são as ondas de dopamina no cérebro causando aumento de tolerância e saciedade. A pornografia destrói o funcionamento normal do cérebro, a masturbação confunde a resposta músculo-cérebro; o orgasmo inunda o cérebro com opióides e torna o caminho mais fácil de seguir na próxima vez.

**O Usuário "Eu parei, mas tenho uma espiada ocasional"**

De certa forma, os usuários espiadores são os mais patéticos de todos. Ou eles passam a vida acreditando que estão sendo privados ou, mais frequentemente, uma espiada ocasional se torna duas. Deslizando para baixo no toboágua escorregadio, mais cedo ou mais tarde voltando a ser usuários pesados. Eles caíram novamente na mesma armadilha em que caíram em primeiro lugar.

Existem duas outras categorias de usuários casuais. O primeiro é o tipo que se masturba com imagens ou vídeos das últimas fitas de sexo de celebridades que chegam ao noticiário, ou algo que elas "carregaram para casa" depois de assistirem "acidentalmente" na escola ou no trabalho. Essas pessoas são apenas não usuários, mas sentem que estão perdendo. Eles querem fazer parte da ação, com a maioria de nós começando assim. Da próxima vez, observe que depois de um tempo a celebridade de sua fantasia não está mais te estimulando tanto. Quanto mais "inatingível" for o alvo de sua fantasia, mais frustrante será a retirada do orgasmo.

A segunda categoria vem ganhando atenção recentemente, e é melhor descrita com a história de um caso compartilhado online.

Uma mulher profissional já lia histórias pornográficas na internet há muitos anos e nunca usava mais ou menos do que uma vez por noite. A propósito, ela era uma senhora muito obstinada. A maioria dos usuários se perguntaria por que ela queria parar em primeiro lugar - alegremente avisando que ela não havia risco de disfunção erétil ou ejaculação precoce no caso dela (o que é uma mentira). Ela nem mesmo estava usando imagens, as histórias eram muito mais inofensivas do que qualquer material que eles próprios usam no dia a dia.

Eles cometem o erro de presumir que os usuários casuais são mais felizes e têm mais controle. Eles podem estar mais no controle, mas certamente não estão felizes. No caso da mulher, ela não estava satisfeita com o parceiro nem com o sexo real e ficava muito irritada para lidar com as tensões e estresses diários. Seu mais próximo e querido era incapaz de descobrir o que a estava incomodando. Mesmo se ela se convencesse a não temer seu uso por meio da racionalização, ela ainda se veria incapaz de desfrutar de relacionamentos reais que invariavelmente envolvem altos e baixos. O centro de recompensa de seu cérebro era incapaz de usar os desestressadores normais presentes na vida como resultado da inundação diária de dopamina. A regulação subsequente dos receptores cerebrais a tornou melancólica na maioria das circunstâncias. Como a maioria, ela tinha um grande medo do lado negro da pornografia e do tratamento dispensado às mulheres - antes de sua primeira vez. Eventualmente, ela foi vítima de uma lavagem cerebral da sociedade e visitou seu primeiro site. Ao contrário da maioria dos que se deixam levar e se tornam usuários pesados - ao ver os vídeos horríveis de violência, ela resistiu à tentação.

Tudo o que você gosta na pornografia é, na verdade, a vontade de acabar com o desejo que começou antes, seja o desejo físico quase imperceptível ou a tortura mental de não poder coçar a coceira. A pornografia na Internet em si é um veneno, por isso você só tem a ilusão de aproveitá-la depois de períodos de abstinência. Da mesma forma que a fome ou a sede, quanto mais você sofrer, maior será o prazer ao ser finalmente aliviado. Cometendo o erro de acreditar que a pornografia é apenas um hábito, eles pensam: "*Se eu puder manter isso em um determinado nível ou apenas em ocasiões especiais, meu cérebro e corpo aceitarão. Então, posso continuar usando esse nível ou reduzi-lo ainda mais, se eu desejar.*"

Deixe claro em sua mente, o "hábito" não existe. A pornografia é um vício em drogas, com a tendência natural de aliviar os sintomas de abstinência, e não de suportá-las. Para mantê-lo no nível em que você está atualmente, seria necessário exercitar uma quantidade enorme de disciplina e força de vontade pelo resto de sua vida; à medida que o centro de recompensa do seu cérebro se torna imune à dopamina e aos opioides, ele quer mais e mais, não menos e menos.

À medida que a pornografia começa a destruir gradualmente seu sistema nervoso, coragem, confiança e controles de impulso, você se torna cada vez mais incapaz de resistir a reduzir o tempo entre cada sessão. É por isso que, nos primeiros dias, podemos pegar ou largar. Se recebêssemos um sinal de que algo está errado mental ou fisicamente, simplesmente paramos. Não tenha inveja dessa mulher, quando você assiste apenas uma vez a cada vinte e quatro horas, parece ser a coisa mais preciosa do mundo, transformando a pornografia em um "fruto proibido". Por muitos anos, essa pobre mulher esteve no centro de um cabo de guerra.

Incapaz de parar de usar, mas com medo de passar para vídeos pornográficos. Por vinte e três horas e dez minutos de cada um daqueles dias, ela teve que lutar contra a tentação e a falta de sentimentos em relação ao namorado. Foi necessária uma tremenda força de vontade para fazer o que ela fez, o que a levou às lágrimas. Esses casos são raros, mas olhe para isso com lógica: ou existe uma muleta emocional genuína, ou prazer na pornografia, ou não. Se houver, quem quer esperar uma hora, um dia ou mesmo uma semana? Por que você deveria ser privado da muleta ou do prazer enquanto isso? Se não há muleta ou prazer genuíno, por que se dar ao trabalho de visitar seu harém online?

Aqui está outro caso de um homem que ocorre uma vez a cada quatro dias, descrevendo sua vida da seguinte forma:

> "*Tenho quarenta anos, sofri PIED (disfunção erétil induzida por pornografia) com mulheres reais e até quando uso pornografia, o que acontece na maioria das vezes. Já faz um tempo que não tenho uma ereção completa. Antes de entrar na dieta de pornografia que ocorre uma vez em quatro, eu costumava dormir profundamente a noite após minha sessão. Agora eu acordo a cada hora da noite e pornografia é tudo em que consigo pensar. Mesmo dormindo, sonho com meus vídeos favoritos. Dias depois da sessão agendada, me sinto bem para baixo, pois a dieta está consumindo toda a minha energia. Minha parceira ia me abandonar porque sou muito mal-humorado e se ela não puder ir embora, não me deixará entrar em casa. Eu faço corridas lá fora, mas minha mente está obcecada com isso.*
>
> *No dia programado, começo a planejar no início da noite, ficando muito irritado se algo acontecer contra meus planos. Eu desistia das conversas e consumia (apenas para me arrepender mais tarde) no trabalho e em casa. Não sou um cara argumentativo, mas não quero que o assunto ou a conversa me atrapalhe. Lembro-me de ocasiões em que escolhia brigas bobas com minha parceira. Eu espero pelas dez horas e quando chega minhas mãos estão tremendo incontrolavelmente. Eu não começo o ato imediatamente - já que novos vídeos foram adicionados - e "dou uma pesquisada". Minha mente me diz que, já que passei fome por quatro dias, mereço um vídeo "especial" que deve valer o tempo gasto procurando. Eventualmente, eu me contento com um ou dois, mas quero que dure para que eu possa "sobreviver" pelos próximos quatro dias, então eu levo mais tempo para terminar o ato.*"  

Além de seus outros problemas, este pobre homem não tem ideia de que está se tratando com veneno. Primeiro, sofrendo da "síndrome do fruto proibido" e, em seguida, forçando seu cérebro a liberar dopamina. Comparativamente, seus receptores de dopamina não estão tão reduzidos, mas ele está engraxando os toboáguas pornográficos, buscando, procurando novidade, variedade, choque e ansiedade para sobreviver aos próximos quatro dias. Você provavelmente imagina esse homem como um imbecil patético, mas não é assim. Ex-atleta que virou ex-sargento da Marinha, não queria ficar viciado em nada. No entanto, ao retornar da guerra, ele treinou como técnico de TI em um programa de reabilitação de veteranos.

Ao entrar no mercado de trabalho civil, ele era um profissional de TI bem pago em um banco e recebeu um laptop para levar para casa. Foi o ano em que socialites famosas "vazaram" seus vídeos pornôs online e muito se falou sobre isso. Ele então foi fisgado, passando o resto de sua vida pagando caro e se arruinando física e mentalmente. Se ele fosse um animal, a sociedade teria acabado com seu sofrimento há muito tempo, mas ainda permitimos que jovens adolescentes fisicamente e mentalmente saudáveis ​​se tornem fisgados. Você pode pensar que este caso e as notas são exagerados, mas este caso - embora extremo - está longe de ser único. Existem dezenas de milhares de histórias semelhantes. Você pode ter certeza de que muitos de seus amigos e conhecidos invejavam ele por ser um homem que só consumia uma vez em cada quatro dias? Se você acha que isso não poderia acontecer com você, **pare de se enganar.**

**JÁ ESTÁ ACONTECENDO.**

Como outros viciados, os usuários de pornografia são notórios mentirosos, até para eles próprios. Eles têm que ser. A maioria dos usuários casuais se entrega muito mais vezes e em muito mais ocasiões do que eles admitem. Muitas conversas com os chamados usuários "duas vezes por semana" admitem que já o fizeram mais de três ou quatro vezes naquela semana. Leia reddit, NoFap e histórias de fóruns de reboot de usuários casuais e você descobrirá que eles estão contando dias ou esperando para falhar. Você não precisa invejar os usuários casuais, também não precisa consumir pornografia, a vida é infinitamente mais doce sem ela. Faça o seguinte registro:

"*Tudo começou com um simples desafio de não consumir o meu lixo por um dia e ser incapaz de fazer isso. Eu não penso mais em masturbação, isso não me passa pela cabeça. Isso é possível, eu prometo a você. As riquezas que aguardam aqueles que são capazes - são incríveis.*"

Os adolescentes geralmente são mais difíceis de curar, não porque achem mais difícil parar, mas porque não acreditam que estão viciados ou estão nos estágios iniciais da armadilha. Geralmente sofrendo da ilusão que eles teriam parado automaticamente antes do segundo estágio.

Os pais de crianças que detestam pornografia na internet não devem ter uma falsa sensação de segurança. Todas as crianças detestam os lados sombrios da pornografia antes de se tornarem viciadas. Em um ponto, você também detestava. Não se deixe enganar por campanhas de medo também, a armadilha é a mesma de sempre. As crianças sabem que a pornografia na internet é um estímulo sobrenatural, mas também sabem que uma "visita" ou "espiada" não faz nada de mal. Em algum estágio, eles podem ser influenciados por um parceiro, colega de classe ou colega de trabalho.

Por favor, não seja complacente com este assunto. O fracasso da sociedade em evitar que os adolescentes se tornem viciados em pornografia na internet e outras drogas é talvez o problema mais perturbador desse vício. Os cérebros dos adolescentes são significativamente mais plásticos e é necessário educá-los e protegê-los. Se você não souber por onde começar, bons recursos incluem o livro YourBrainOnPorn para se educar na neurociência. Mesmo que você suspeite que seu filho já esteja viciado, o livro fornece uma compreensão fundamental para ajudar alguém a escapar. Caso contrário, recomende este livro!
